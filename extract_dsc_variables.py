import os

dirname = (os.path.dirname(os.path.abspath(__file__)))
print(dirname)

project_name = (dirname.split('/')[-1])

try:
    device_server_name = project_name.split("-")[-1]
except IndexError:
    device_server_name = project_name

print(device_server_name)

with open('.env', 'w') as writer:
    # writer.write(f'export DSC_DEVICE_SERVER_NAME="{device_server_name}"')
    writer.write(f'export TEST_VAR="ala ma kota"')